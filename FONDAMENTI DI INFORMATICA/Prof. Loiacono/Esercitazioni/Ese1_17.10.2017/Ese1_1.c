/* Chiedere all'utente di inserire un numero interno n non negativo e stampare a video l'ennnesimo elemento della successione di Fibonacci

La successione di Fibonacci è la seguente:
F(0) = 0
F(1) = 1
F(2) = 1
F(n) = F(n-1) + F(n-2) per ogni n>=3      (n-1) e (n-2) sono a pedice
*/

#include <stdio.h>

int main()
{
    //fib -> F(n)
    //prec1 -> F(n-1)
    //prec2 -> F(n-2)
    int n, fib, prec1, prec2, i;

    do {
        printf("Inserisci n: ");
        scanf("%d%*c", &n);
    } while(n<0);

    prec1 = 1;
    prec2 = 0;
    fib = 0;

    if(n==1) {
        fib = 1;
    } else {
        for(i=2; i<=n; i++) { //i=2 perchè per 0 e 1 sono già stati calcolati (valgono 0 e 1 per definizione)
            fib = prec1 + prec2;
            prec2 = prec1; //aggiorno i valori --> quello che era F(n-1) diventa F(n-2)
            prec1 = fib;
        }
    }

    printf("Risultato: %d\n", fib);

    return 0;
}
