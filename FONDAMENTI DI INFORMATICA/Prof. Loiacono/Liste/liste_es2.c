#include "liste_char.h"
#include <time.h>
#include <string.h>

lista string2list(char str[]);
int palindroma(lista l);

int main()
{
  char str[100];
  lista l;
  scanf("%s",str);
  l = string2list(str);
  printf ("La lista: ");
  stampa(l,1);
  if (palindroma(l)==0)
    printf ("non");
  printf(" e' palindroma.\n");
  return 0;
}

lista string2list(char str[])
{
  int i;
  lista l=NULL;
  for (i=strlen(str)-1; i>=0; i--)
  {
    l = inserisci_in_testa(l,str[i]);
  }
  return l;
}

int palindroma(lista l)
{
  lista inv=NULL,cur=l;
  while (cur!=NULL)
  {
    inv = inserisci_in_testa(inv,cur->el);
    cur = cur->next;
  }
  while (l!=NULL && l->el == inv->el)
  {
    l = l->next;
    inv = inv->next;
  }
  return l==NULL;
}
