/*

Implementare una funzione ricorsiva in C che riceve in ingresso un array a di interi e restituisce 1 se ogni elemento di a è maggiore od uguale dei precedenti,
altrimenti restituisce 0.

Esempi: se a[] = {2,4,5,28,90}, la funzione restituisce 1; se a[] = {5,16,2,82,99}, la funzione restituisce 0.

Nota: La funzione dovrà avere l’array a come parametro in ingresso; è possibile introdurre ulteriori parametri di ingresso se necessari alla corretta implementazione della funzione


*/

int crescente(int a[], int n) { //n è il numero di elementi di a
    if(n == 1)
        return 1;
    else
        return (a[0]<=a[1]) && crescente(a+1,n-1);
}
